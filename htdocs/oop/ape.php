<?php  
require_once ('animal.php');
class Ape extends Animal
{
    public $name = 'Kera Sakti';
    public $legs = 2;
    public $yell = 'Auooo';
    public function get_yell()
    {
        return $this->yell;
    }
}
?>