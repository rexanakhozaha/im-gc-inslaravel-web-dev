<?php
require_once ('animal.php');
require_once ('ape.php');
require_once ('frog.php');

$sheep = new animal();

echo "Name: ".$sheep->get_name();
echo "<br>";
echo "legs: ".$sheep->get_legs();
echo "<br>";
echo "cold blooded: ".$sheep->get_cold_blooded();
echo "<br>";
echo "<br>";

$kodok = new frog();
echo "Name: ".$kodok->get_name();
echo "<br>";
echo "legs: ".$kodok->get_legs();
echo "<br>";
echo "cold blooded: ".$kodok->get_cold_blooded();
echo "<br>";
echo "jump : ".$kodok->get_jump();
echo "<br>";
echo "<br>";

$sungokong = new Ape();
echo "Name: ".$sungokong->get_name();
echo "<br>";
echo "legs: ".$sungokong->get_legs();
echo "<br>";
echo "cold blooded: ".$sungokong->get_cold_blooded();
echo "<br>";
echo "Yell: ".$sungokong->get_yell();
 ?>
